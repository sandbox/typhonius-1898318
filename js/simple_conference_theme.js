/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
  $(document).ready(function() {
  // click event: toggle visibility of group clicked (and update icon)
  // .view-content > .view-grouping
    // $(".national-conference-sessions h3").click(function() {
    $(".national-conference-sessions .view-content > .view-grouping > .view-grouping-header").click(function() {
      var icon = $(this).children(".collapse-icon");
      $(this).siblings(".view-grouping-content").slideToggle(function() {
       (icon.text()=="►" && $(this).is(':visible')) ? icon.text("▼") : icon.text("►");
      });
    });

    // Expand the menu when icon clicked
    $("#menu-lines").click(function() {
      $('#footer ul.menu').slideToggle('slow');
    });
  });


})(jQuery, Drupal, this, this.document);
